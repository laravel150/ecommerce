<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Email - Inscription</title>
</head>

<body>
    <div style="font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2">
        <div style="margin:50px auto;width:70%;padding:20px 0">
            <div style="border-bottom:1px solid #eee">
                <a href="" style="font-size:1.4em;color: #00466a;text-decoration:none;font-weight:600">Inscription
                </a>
            </div>
            <p style="font-size:1.1em">{{ $details['user'] }}</p>
            <p>Merci d'avoir choisi <b>e-commerce App</b></p>
            <p>Vos informations de connexion: </p>
            <p><b>Email:</b> {{ $details['email'] }}</p>
            <p><b>Mot de passe:</b> {{ $details['mdp'] }}</p>
            <p style="font-size:0.9em;">Regards,<br />Votre commercial</p>
            <hr style="border:none;border-top:1px solid #eee" />
            <div style="float:right;padding:8px 0;color:#aaa;font-size:0.8em;line-height:1;font-weight:300">
                <p>Ecommerce - App</p>
                <p>Abidjan</p>
                <p>Marcory, Zone 4</p>
            </div>
        </div>
    </div>
</body>

</html>
