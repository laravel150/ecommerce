@extends('front.layouts.master', [
    'title' => 'Connexion',
    'sub_title' => '',
])
@section('content')
    <!-- CONTENT -->
    <div id="sns_content" class="wrap layout-m">
        <div class="container">
            <div class="row">
                <div id="contact_gmap" class="col-md-12">
                    <div class="page-title">
                        <h1>Contact Us</h1>
                    </div>
                    <div class="row clearfix">
                        <div class="col-md-4 contact-info">
                            <p>Lorem Ipsum has been the industry's standard dummy text
                                ever since.Lorem Ipsum is simyp.</p>
                            <ul class="fa-ul">
                                <li><i class="fa-li fa fa-map-marker"></i>5 Avenue Anatole France 75007</li>
                                <li><i class="fa-li fa fa-phone"></i>+00-123-456-789</li>
                                <li><i class="fa-li fa fa-envelope-o"></i><a
                                        href="mailto:contact@paris.com">info@yourdomain.com</a></li>
                            </ul>
                        </div>
                        <div class="col-md-8">
                            <p class="style1">Send an email. All fields with an (*) are required.</p>
                            <form id="contactForm" action="{{ route('front.login') }}" method="POST">
                                @csrf
                                @method('post')
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input name="email"
                                                class="form-control checkbox required-entry validate-email" id="email"
                                                placeholder="E-mail (*)" title="Email" value="{{ old('email') }}"
                                                type="text" />
                                            @error('email')
                                                <strong class="text-danger">{{ $message }}</strong>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <input class="input-text form-control" name="password" id="pwd1"
                                                placeholder="Mot de passe" title="Adresse" value="" type="password" />
                                            @error('password')
                                                <strong class="text-danger">{{ $message }}</strong>
                                            @enderror
                                        </div>
                                        <button class="btn btn-secondary" type="submit">Valider</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- AND CONTENT -->
@endsection
