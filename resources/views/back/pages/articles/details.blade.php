@extends('back.layouts.master', [
    'title' => 'Articles',
    'sub_title' => 'Détails',
])
@section('content')
<div class="breadcrumbs">
    <div class="breadcrumbs-inner">
        <div class="row m-0">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Articles</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="{{ route('back.dashboard') }}">Tableau de bord</a></li>
                            <li><a href="{{ route('back.articles.index') }}">Articles</a></li>
                            <li class="active">Détails</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content pb-0">
@include('back.layouts.notifications')
    <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title mb-3">Détails</strong>
                </div>
                <div class="card-body">
                    <div class="mx-auto d-block">
                        <h3 class="text-sm-center mt-2 mb-1">{{ $article->name }}</h3>
                        <h3 class="text-sm-center mt-2 mb-1">Prix: {{ $article->price .' €' }}</h3>
                        <h5 class="text-sm-center mt-2 mb-1"> Catégorie: {{ $article->categorie->name }}</h5>
                    </div>
                    <hr>
                    <div class="card-text text-sm-center">
                        <p>{{ $article->description }}</p>
                    </div>
                </div>
            </div>
    </div>
</div> <!-- .content -->
@endsection
