@extends('back.layouts.master', [
    'title' => 'Articles',
    'sub_title' => 'Modifier',
])
@section('content')
<div class="breadcrumbs">
    <div class="breadcrumbs-inner">
        <div class="row m-0">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Articles</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="{{ route('back.dashboard') }}">Tableau de bord</a></li>
                            <li class="active">Articles</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content pb-0">
@include('back.layouts.notifications')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header"><small> Formulaire de Modification</small>  <strong> Articles</strong></div>
            <form action="{{ route ('back.articles.update', $article->id) }}" method="post" enctype="multipart/form-data">
               @csrf
               @method('patch')
                <div class="card-body card-block">
                    <div class="form-group">
                        <label for="picture" class=" form-control-label">Photo Article</label> <br/>
                        <input type="file" name="picture" class=""  />
                        @error('picture')
                            <strong class="text-danger">{{$message}}</strong>
                        @enderror
                    </div>
                    <div class="form-group"><label for="name" class=" form-control-label">Nom</label>
                        <input type="text" id="name" name="name" placeholder="" class="form-control" value="{{ $article->name}}" required>
                        @error('name')
                            <strong class="text-danger">{{$message}}</strong>
                        @enderror
                    </div>
                    <div class="form-group"><label for="categories" class=" form-control-label">Catégorie d'articles</label>
                        <select name="categories" id="categories" class="form-control">
                           @foreach ($categories as $categorie)
                                <option value={{$categorie->id}} {{ $article->categorie->id ==  $categorie->id ? 'selected' : '' }}>{{$categorie->name}}</option>
                           @endforeach
                        </select>
                        @error('categories')
                            <strong class="text-danger">{{$message}}</strong>
                        @enderror
                    </div>
                    <div class="form-group"><label for="price" class=" form-control-label">Prix</label>
                        <input type="number" id="price" name="price" placeholder="" class="form-control" value="{{ $article->price }}" required>
                        @error('price')
                            <strong class="text-danger">{{$message}}</strong>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="description" class=" form-control-label">Description</label>
                        <textarea type="text" id="description" placeholder="" name="description" class="form-control" required>{{ $article->description }}</textarea>
                    </div>
                    <a href="{{ route ('back.articles.index') }}" class="btn btn-secondary">ANNULER</a>
                    <button class="btn btn-success">VALIDER</button>
                </div>
            </form>
        </div>
    </div>
</div> <!-- .content -->
@endsection