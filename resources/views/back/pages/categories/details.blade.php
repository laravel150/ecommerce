@extends('back.layouts.master', [
    'title' => 'Catégories',
    'sub_title' => 'Détails',
])
@section('content')
<div class="breadcrumbs">
    <div class="breadcrumbs-inner">
        <div class="row m-0">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Catégories</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="{{ route('back.dashboard') }}">Tableau de bord</a></li>
                            <li><a href="{{ route('back.categories.index') }}">Categories</a></li>
                            <li class="active">Détails</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content pb-0">
@include('back.layouts.notifications')
    <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title mb-3">Détails</strong>
                </div>
                <div class="card-body">
                    <div class="mx-auto d-block">
                        <h5 class="text-sm-center mt-2 mb-1">{{ $category->name }}</h5>
                    </div>
                    <hr>
                    <div class="card-text text-sm-center">
                        <p>{{  $category->description}}</p>
                    </div>
                </div>
            </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Liste des articles associés ( {{count($category->articles)}} )</strong>
            </div>
            <div class="card-body">
                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Description</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($category->articles as $article)
                        <tr>
                            <td>{{ $article->name }}</td>
                            <td>{{ $article->description }}</td>
                            <td>{{ $article->price. ' €' }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> <!-- .content -->
@endsection
