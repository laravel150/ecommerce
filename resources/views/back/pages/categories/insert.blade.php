@extends('back.layouts.master', [
    'title' => 'Catégories',
    'sub_title' => 'Ajouter',
])
@section('content')
<div class="breadcrumbs">
    <div class="breadcrumbs-inner">
        <div class="row m-0">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Catégories</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="{{ route('back.dashboard') }}">Tableau de bord</a></li>
                            <li class="active">Catégories</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content pb-0">
@include('back.layouts.notifications')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header"><small> Formulaire</small>  <strong> Catégories</strong></div>
            <form action="{{ route ('back.categories.store')}}" method="post">
               @csrf
               @method('post')
                <div class="card-body card-block">
                    <div class="form-group"><label for="name" class=" form-control-label">Nom</label>
                        <input type="text" id="name" name="name" placeholder="" class="form-control" value="{{ old('name')}}" required>
                        @error('name')
                            <strong class="text-danger">{{$message}}</strong>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="description" class=" form-control-label">Description</label>
                        <textarea type="text" id="description" placeholder="" name="description" class="form-control" required>{{ old('description')}}</textarea>
                        @error('description')
                        <strong class="text-danger">{{$message}}</strong>
                        @enderror
                    </div>
                    <a href="{{ route ('back.categories.index') }}" class="btn btn-secondary">ANNULER</a>
                    <button class="btn btn-success">VALIDER</button>
                </div>
            </form>
        </div>
    </div>
</div> <!-- .content -->
@endsection
