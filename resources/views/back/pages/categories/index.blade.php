@extends('back.layouts.master', [
    'title' => 'Catégories',
    'sub_title' => 'Liste',
])
@section('content')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Catégories</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="{{ route('back.dashboard') }}">Tableau de bord</a></li>
                                <li class="active">Catégories</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content pb-0">
        @include('back.layouts.notifications')
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('back.categories.create') }}" class="btn btn-success" style="float: right"> <i class="fa fa-plus"></i>  Ajouter</a><br/>
                    <br style="clear: both">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Liste</strong>
                        </div>
                        <div class="card-body">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Description</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($categories as $category)
                                        <tr>
                                            <td>{{ $category->name }}</td>
                                            <td>{{ $category->description }}</td>
                                            <td style="text-align:center">
                                                <a href=" {{ route('back.categories.show', $category->id) }}"><i
                                                        class=" fa fa-eye" title="Plus de détails"></i></a>
                                                <?php echo '&nbsp;&nbsp'; ?>
                                                <a href=" {{ route('back.categories.edit', $category->id) }}"><i
                                                        class=" fa fa-edit" title="Modifier"></i></a>
                                                <?php echo '&nbsp;&nbsp'; ?>
                                                <a href="#" data-toggle="modal"
                                                    data-target="#suppressionModal{{ $category->id }}"><i
                                                        class=" fa fa-trash" title="Supprimer"></i></a>
                                            </td>
                                        </tr>

                                        <div class="modal fade" id="suppressionModal{{ $category->id }}" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header" style="background-color:#dc3545; color:white">
                                                        <h5 class="modal-title" id="exampleModalLabel">Confirmation
                                                            suppression</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p class="text-danger">Voulez vraiment supprimer cette catégorie <b>{{ $category->name }}</b> ?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Fermer</button>
                                                        <a href="{{ route('back.categories.delete', $category->id) }}"
                                                            class="btn btn-danger">Supprimer</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div><!-- .animated -->
    </div> <!-- .content -->
@endsection
