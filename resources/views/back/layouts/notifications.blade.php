@if ($message = Session::get('success'))
    <div class="alert alert-success alert-dismissible" role="alert">
        {{-- <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button> --}}
        <div class="alert-message">
            <strong>{!! $message !!}</strong>
        </div>
    </div>
    {{ session()->forget('success') }}
@endif
@if ($message = Session::get('warning'))
    <div class="alert alert-warning alert-dismissible" role="alert">
        {{-- <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button> --}}
        <div class="alert-message">
            <strong>{!! $message !!}</strong>
        </div>
    </div>
    {{ session()->forget('warning') }}
@endif
@if ($message = Session::get('danger'))
    <div class="alert alert-danger alert-dismissible" role="alert">
        {{-- <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button> --}}
        <div class="alert-message">
            <strong>{!! $message !!}</strong>
        </div>
    </div>
    {{ session()->forget('danger') }}
@endif

@error('email')
<div class="alert alert-warning alert-dismissible" role="alert">
    {{-- <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button> --}}
    <div class="alert-message">
        <strong>Désolé, {{{ old('email') }}} existe déjà !</strong>
    </div>
</div>
{{ session()->forget('email') }}
@enderror