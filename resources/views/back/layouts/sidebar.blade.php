<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="{{route('back.dashboard')}}"><i class="menu-icon fa fa-laptop"></i>Tableau de bord </a>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Catégories</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-align-justify"></i> <a href="{{ route('back.categories.index') }}">Liste</a></li>                        
                        <li><i class="fa fa-plus"></i><a href="{{ route('back.categories.create') }}">Ajouter</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false"> <i class="menu-icon fa fa-book"></i>Articles</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-align-justify"></i> <a href="{{ route('back.articles.index') }}">Liste</a></li>                        
                        <li><i class="fa fa-plus"></i><a href="{{ route('back.articles.create') }}">Ajouter</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside><!-- /#left-panel -->
