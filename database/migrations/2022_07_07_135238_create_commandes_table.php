<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commandes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('qte');
            $table->dateTime('date');

            $table->bigInteger('id_customer')->unsigned();
            $table->bigInteger('id_article')->unsigned();

            $table->foreign('id_customer')->references('id')->on('customers')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('id_article')->references('id')->on('articles')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commandes', function (Blueprint $table) {
            $table->dropForeign('commandes_id_customer_foreign');
            $table->dropForeign('commandes_id_article_foreign');
        });
    }
}