<?php

use App\Http\Controllers\Back\ArticleController;
use App\Http\Controllers\Back\Auth\AdminLoginController;
use App\Http\Controllers\Back\CategoryController;
use App\Http\Controllers\Back\DashboardController;
use App\Http\Controllers\Front\ClientLoginController;
use App\Http\Controllers\Front\ClientRegisterController;
use App\Http\Controllers\Front\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home.index');

// Auth - admin
Route::get('/auth/login', [AdminLoginController::class, 'showLoginForm'])->name('back.login');
Route::post('/auth/login', [AdminLoginController::class, 'login'])->name('back.login');
Route::post('/auth/logout', [AdminLoginController::class, 'logout'])->name('back.logout');

Route::prefix('admin')->middleware('auth')->group(function () {
    Route::get('/tableau-de-bord', [DashboardController::class, 'index'])->name('back.dashboard');

    Route::get('/categories/index', [CategoryController::class, 'index'])->name('back.categories.index');
    Route::get('/categories/create', [CategoryController::class, 'create'])->name('back.categories.create');
    Route::post('/categories/store', [CategoryController::class, 'store'])->name('back.categories.store');
    Route::get('/categories/show/{id}', [CategoryController::class, 'show'])->name('back.categories.show');
    Route::get('/categories/edit/{id}', [CategoryController::class, 'edit'])->name('back.categories.edit');
    Route::patch('/categories/{id}', [CategoryController::class, 'update'])->name('back.categories.update');
    Route::get('/categories/{id}', [CategoryController::class, 'delete'])->name('back.categories.delete');

    Route::get('/articles/index', [ArticleController::class, 'index'])->name('back.articles.index');
    Route::get('/articles/show/{id}', [ArticleController::class, 'show'])->name('back.articles.show');
    Route::get('/articles/create', [ArticleController::class, 'create'])->name('back.articles.create');
    Route::post('/articles/store', [ArticleController::class, 'store'])->name('back.articles.store');
    Route::get('/articles/edit/{id}', [ArticleController::class, 'edit'])->name('back.articles.edit');
    Route::patch('/articles/{id}', [ArticleController::class, 'update'])->name('back.articles.update');
    Route::get('/articles/{id}', [ArticleController::class, 'delete'])->name('back.articles.delete');
});

// Auth customer
Route::get('/client/inscription', [ClientRegisterController::class, 'create'])->name('front.register');
Route::post('/client/inscription', [ClientRegisterController::class, 'store'])->name('front.store');

Route::get('/client/connexion', [ClientLoginController::class, 'showLoginForm'])->name('front.login');
Route::post('/client/connexion', [ClientLoginController::class, 'login'])->name('front.login');


// Route::get('/email', function () {
//     $details = [
//         'user' => 'Nom Prénoms',
//         'email' => 'email.com',
//         'mdp' => '1234',
//     ];

//     return view('front.mailInscription', compact('details'));
// });