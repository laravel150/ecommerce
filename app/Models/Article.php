<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Article extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'articles';

    protected $fillable =
    [
        'name',
        'description',
        'price',
        'id_category',
        'picture'
    ];

    public function categorie()
    {
        return $this->belongsTo(Category::class, 'id_category');
    }

    public function commandes()
    {
        return $this->belongsToMany(Article::class, 'commandes', 'id_customer', 'id_article')->withTimestamps();
    }
}