<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Customer extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'customers';

    protected $fillable =
    [
        'name',
        'lastname',
        'sex',
        'email',
        'phone',
        'adress',
        'password'
    ];

    public function commandes()
    {
        return $this->belongsToMany(Customer::class, 'commandes', 'id_article', 'id_customer')->withTimestamps();
    }
}