<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Client extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];
    protected $table = 'clients';

    protected $hidden = [
        'password', 'remember_token',
    ];


    protected $fillable =
    [
        'name',
        'lastname',
        'sex',
        'email',
        'phone',
        'address',
        'email_verified_at',
        'password'
    ];
}