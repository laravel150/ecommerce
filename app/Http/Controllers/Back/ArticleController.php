<?php

namespace App\Http\Controllers\Back;

use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ArticleController extends Controller
{
    public function index()
    {
        $articles = Article::orderBy('created_at', 'desc')->get();
        return view('back.pages.articles.index', compact('articles'));
    }

    public function show($id)
    {
        try {
            $article = Article::find($id);
            return view('back.pages.articles.details', compact('article'));
        } catch (\Throwable $th) {
            report($th);
            session()->flash('danger', 'Une erreur s\'est produite');
            return redirect()->back()->withInput();
        }
    }

    public function create()
    {
        $categories = Category::orderBy('created_at', 'desc')->get();
        return view('back.pages.articles.insert', compact('categories'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'string|required|min:2|max:100|unique:articles,name,NULL,id,deleted_at,NULL',
            'price' => 'required|min:1|max:1000',
            'description' => 'string|min:2|max:1000',
            'picture' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10240'

        ]);

        $image = Storage::disk('public')->put('products', $request->file('picture'));

        try {
            $article = Article::create([
                'name' => $request->name,
                'description' => $request->description,
                'price' => $request->price,
                'id_category' => $request->categories,
                'picture' => $image,

            ]);

            if ($article) {
                session()->flash('success', 'Article enregistré avec succès!');
                return redirect()->route('back.articles.index');
            }
        } catch (\Throwable $th) {
            report($th);
            session()->flash('danger', 'Une erreur s\'est produite lors de l\'enregistrement de la catégorie !');
            return redirect()->back()->withInput();
        }
    }

    public function edit($id)
    {
        try {
            $categories = Category::orderBy('created_at', 'desc')->get();
            $article = Article::find($id);
            return view('back.pages.articles.edit', compact('article', 'categories'));
        } catch (\Throwable $th) {
            report($th);
            session()->flash('danger', 'Une erreur s\'est produite');
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        $article = Article::find($id);

        $request->validate([
            'name' => 'string|required|min:2|max:100',
            'price' => 'required|min:1|max:1000',
            'description' => 'string|min:2|max:1000',
            'picture' => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240'

        ]);
        if (!($request->file('picture'))) {
            $image = $article->picture;
        } else {
            $image = Storage::disk('public')->put('products', $request->file('picture'));
        }

        try {
            $article->name = $request->name;
            $article->description = $request->description;
            $article->price = $request->price;
            $article->picture = $image;


            if ($article->update()) {
                session()->flash('success', 'Article modifé avec succès!');
                return redirect()->route('back.articles.index');
            }
        } catch (\Throwable $th) {
            report($th);
            session()->flash('danger', 'Une erreur s\'est produite lors de l\'enregistrement de la catégorie !');
            return redirect()->back()->withInput();
        }
    }


    public function delete($id)
    {
        try {
            $article = Article::find($id);

            if ($article->delete()) {

                if (Storage::exists('public/' . $article->picture)) {
                    Storage::delete('public/' . $article->picture);
                }

                session()->flash('success', 'Suppression réussie!');
                return redirect()->route('back.articles.index');
            } else {
                session()->flash('warning', "La suppression n'a pas pu être effectuée !");
                return redirect()->back();
            }
        } catch (\Throwable $th) {
            report($th);
            session()->flash('danger', 'Une erreur s\'est produite');
            return redirect()->back();
        }
    }
}