<?php

namespace App\Http\Controllers\Back;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    
    public function index()
    {
        $categories = Category::orderBy('created_at', 'desc')->get();
        return view ('back.pages.categories.index', compact('categories'));
    }

    public function create()
    {
        return view ('back.pages.categories.insert');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'string|required|min:2|max:100|unique:categories,name,NULL,id,deleted_at,NULL',
            'description' => 'string|required|min:2|max:1000',
        ]);

        try {
            $category = Category::create([
                'name' => $request->name,
                'description' => $request->description
            ]);

            if ($category) {
                session()->flash('success', 'Catégorie enregistrée avec succès!');
                return redirect()->back();
            }
        } catch (\Throwable $th) {
            report($th);
            session()->flash('danger', 'Une erreur s\'est produite lors de l\'enregistrement de la catégorie !');
            return redirect()->back()->withInput();
        }
    }

    public function show($id)
    {
        try {
            $category = Category::find($id);
            return view ('back.pages.categories.details', compact('category'));

        } catch (\Throwable $th) {
            report ($th);
            session()->flash('danger', 'Une erreur s\'est produite');
            return redirect()->back()->withInput();
        }
    }

    public function edit($id)
    {
        try {
            $category = Category::find($id);
            return view ('back.pages.categories.edit', compact('category'));

        } catch (\Throwable $th) {
            report ($th);
            session()->flash('danger', 'Une erreur s\'est produite');
            return redirect()->back()->withInput();
        }
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'string|required|min:2|max:100|unique:categories,name,NULL,id,deleted_at,NULL',
            'description' => 'string|required|min:2|max:1000',
        ]);

        try {
            $category = Category::find($id);
            $category->name = $request->name;
            $category->description = $request->description;
            

            if ($category->update()) {
                session()->flash('success', 'Catégorie modifée avec succès!');
                return redirect()->route('back.categories.index');
            }
        } catch (\Throwable $th) {
            report($th);
            session()->flash('danger', 'Une erreur s\'est produite lors de l\'enregistrement de la catégorie !');
            return redirect()->back()->withInput();
        }
    }

    public function delete($id)
    {
        try {
            $category = Category::find($id);
            $category->articles()->delete();
            
             if($category->delete())
                {
                    session()->flash('success', 'Suppression réussie!');
                    return redirect()->route('back.categories.index');
                }
                
        } catch (\Throwable $th) {
            report($th);
            session()->flash('danger', 'Une erreur s\'est produite');
            return redirect()->back();        }
        

        
    }
}
