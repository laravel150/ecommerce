<?php

namespace App\Http\Controllers\Back\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Auth\LoginController;

class AdminLoginController extends LoginController
{
    protected $redirectTo = "/admin/tableau-de-bord";

    public function __construct()
    {
    }

    public function showLoginForm()
    {
        return view('back.pages.auth.login');
    }

    public function username()
    {
        return 'email';
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('back.login');
    }
}