<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Auth\LoginController;

class ClientLoginController extends LoginController
{
    protected $redirectTo = "/";

    public function __construct()
    {
    }

    public function showLoginForm()
    {
        return view('front.login');
    }

    public function username()
    {
        return 'email';
    }

    public function logout(Request $request)
    {
        Auth::guard('clients')->logout();
        return redirect()->route('front.login');
    }

    protected function guard()
    {
        return Auth::guard('clients');
    }
}