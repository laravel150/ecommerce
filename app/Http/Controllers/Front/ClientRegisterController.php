<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Mail\InscriptionMail;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class ClientRegisterController extends Controller
{
    public function create()
    {
        return view('front.register');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'string|required|min:1|max:100',
            'lastname' => 'required|min:1|max:1000',
            'email' => 'string|email|min:1|max:1000|unique:clients,email,NULL,id,deleted_at,NULL',
            'sex' => 'string|min:1',
            'phone' => 'string|min:10|max:1000|unique:clients,phone,NULL,id,deleted_at,NULL',
            'address' => 'string|min:1|max:1000',
            'password_1' => 'string|min:8|max:1000',
            'password_2' => 'required_with:password_1|same:password_1|min:8'
        ]);

        try {

            $client = Client::create([
                'name' => $request->name,
                'lastname' => $request->lastname,
                'sex' => $request->sex,
                'email' => $request->email,
                'phone' => $request->phone,
                'address' => $request->address,
                'password' => Hash::make($request->password_1)
            ]);

            $details = [
                'user' => $request->name . ' ' . $request->lastname,
                'email' => $request->email,
                'mdp' => $request->password_1
            ];

            Mail::to($request->email)->send(new InscriptionMail($details));

            session()->flash('success', 'Votre inscription est faite !');
            return redirect()->route('front.login');
        } catch (\Throwable $th) {
            dd($th);
            session()->flash('danger', 'Une erreur s\'est produite lors de votre inscription !');
            return redirect()->back()->withInput();
        }
    }
}